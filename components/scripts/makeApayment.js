



$('.choose-payment-items .items  label').on('click', function(){
    $(".choose-payment-items .items").removeClass("active");
    $(this).closest(".items").addClass("active")
});

$('.radios-items-choose .items  label').on('click', function(){
    $(".radios-items-choose .items").removeClass("active");
    $(this).closest(".items").addClass("active")
});

$('.payment-forms table.table-border tr').click(function() {
    $(".payment-forms table.table-border tr").removeClass("active");
    $(this).find('td input[type=radio]').prop('checked', true);
    $(this).addClass("active");


    var isCheckbox =$(this).find('td input[type=checkbox]').prop('checked');
    //Checkbox Tablosu içn geçerli
    if(isCheckbox){
        $(this).find('td input[type=checkbox]').prop('checked', false);
        $(this).removeClass("active");
    }
    else{
        $(this).find('td input[type=checkbox]').prop('checked', true);
        $(this).addClass("active");
    }

});


$(".credit-entery .textbox-inside-label label").click(function () {
    var isChecked = $(".textbox-inside-label input[type=checkbox]").prop("checked");
    if(isChecked){
        $(this).removeClass("checked")
    }
    else{
        $(this).addClass("checked")
    }

    $(this).next().toggle();
});


$(".additionPaymentToggle").change(function () {
    $(".addition-payment").toggle();
    $(".nextbtn").toggle();
});


$(".paymentForm-list .title").click(function () {
  $(this).next(".payment-type").stop().slideToggle("slow");
    $(this).toggleClass("active")
});


///Kart Ekleme sayfası. Butona tıklandığında append edilir.
$(".card-add-btn a, a.card-add-btn").click(function () {
    $.ajax({
        type: "GET",
        url: "cardAdd.html",
        dataType : 'html',
        cache: false,
        success : function(data){

            $('#card-add-content').append(data);
            $("body").addClass("body-no-scroll");
            $('.placeholder-animation').placeholder({
                activeClass: 'text-input-placeholder_pos_top'
            });
            new Card({
                form: document.querySelector('form'),
                container: '.card-wrapper'
            });
            cardClose();
            masterpassChange();
            valid();
            mask();
        }
    });
});

function cardClose() {
    //Kart alanını kapatmak için.
    $(".card-close a.btn-card-close").click(function () {
        $('#card-add-content').html("");
        $("body").removeClass("body-no-scroll");
    });
}

function masterpassChange() {
    $(".card-container .more-ways-add").click(function () {
        $(".card-container .manuel-entry").hide();
        $(".card-container .masterpass").show();
        clear_form_elements();
    });

    $(".card-container .back-manuel-entry").click(function () {
        $(".card-container .manuel-entry").show();
        $(".card-container .masterpass").hide();
        clear_form_elements();
    });

}

function clear_form_elements() {

    $('#cardAddForm').find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

    $(".jp-card-display,.jp-card-logo").html("");
    $("#cardAddForm label.text-input-placeholder.js-placeholder-text").removeClass("text-input-placeholder_pos_top");

}


$(".addcard-modal").click(function () {
    $.ajax({
        type: "GET",
        url: "addcard-modal.html",
        dataType : 'html',
        cache: false,
        success : function(data){
            $('body').append(data);
            $("#addCardModal").modal();
        }
    });
});



//Mobilde step sayısını azaltmak.
if(device.any()){
    switch ($(".step-tabs li.active").index()) {
        case 3:
            $(".step-tabs li").eq(0).hide();
            break;
        case 4:
            $(".step-tabs li").eq(0).hide();
            $(".step-tabs li").eq(1).hide();
            break;
        case 5:
            $(".step-tabs li").eq(0).hide();
            $(".step-tabs li").eq(1).hide();
            $(".step-tabs li").eq(2).hide();
            break;
        case 6:
            $(".step-tabs li").eq(0).hide();
            $(".step-tabs li").eq(1).hide();
            $(".step-tabs li").eq(2).hide();
            break;
    }
}

