



$(".singUp-select input[type=radio]").change(function (elem) {


   if($(this).val() == "Corporate"){
       $(".center-form").show().animate({opacity: "1"});
       $(".first-form").animate({opacity: "0"}).hide();
       $(".last-form").animate({opacity: "0"}).hide();
       $(".singUpCt").addClass("fullCt")

   }else if($(this).val() == "individual"){
       $(".last-form").animate({opacity: "0"}).hide();
       $(".center-form").animate({opacity: "0"}).hide();
       $(".first-form").show().animate({opacity: "1"});
       $(".singUpCt").removeClass("fullCt");

   }
   else{
       $(".last-form").show().animate({opacity: "1"});
       $(".first-form").animate({opacity: "0"}).hide();
       $(".center-form").animate({opacity: "0"}).hide();
       $(".singUpCt").addClass("fullCt")
   }

});


$(".password-modal").click(function () {
    $.ajax({
        type: "GET",
        url: "password-modal.html",
        dataType : 'html',
        cache: false,
        success : function(data){
            $('body').append(data);
            $("#passwordModal").modal();

            $(".pass").keyup(function () {
                $(this).parent().next().find(".pass").focus();
            });
        }
    });
});