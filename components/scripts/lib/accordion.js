module.exports = function () {

    var functions = {
        menuAccordion: function () {
            $('.header .accordion-header[accordion-show]').on('click', function () {

                var elem = $(this);

                if ($(elem).attr('accordion-show') === 'false') {
                    $('.header .accordion-header[accordion-show], .header .article[accordion-show]').attr('accordion-show', false);
                    $(elem).attr('accordion-show', true);
                    $(elem).siblings('.article[accordion-show]').attr('accordion-show', true);
                } else {
                    $('.header .accordion-header[accordion-show], .header .article[accordion-show]').attr('accordion-show', false);
                    $(elem).attr('accordion-show', false);
                    $(elem).siblings('.article[accordion-show]').attr('accordion-show', false);
                }

                $('html, body').animate({
                    scrollTop: $(elem).offset().top
                }, 100);


            });
        },
        sidebarSummaryAccordion :function () {
            $('.sidebar-summary .accordion-header[accordion-show]').on('click', function () {

                var elem = $(this);

                if ($(elem).attr('accordion-show') === 'false') {
                    $(elem).attr('accordion-show', true);
                    $(elem).siblings('.article[accordion-show]').attr('accordion-show', true);
                } else {
                    $(elem).attr('accordion-show', false);
                    $(elem).siblings('.article[accordion-show]').attr('accordion-show', false);
                }


            });
        }
    };


    if(device.any()){
        functions.menuAccordion();
    }

    functions.sidebarSummaryAccordion();
    return functions;

};


