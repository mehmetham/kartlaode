module.exports = function () {

    var functions = {
        headerMenu: function () {
            $("#headerUserMenu li").each(function () {
                 $(this).find("a.accordion-header").attr("href", "#");
            });
        }
    };

    if(device.any()){
        functions.headerMenu();
    }
    return functions;

};


