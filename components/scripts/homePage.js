// for general used methods



require('./lib/flexslider/index');


$(".password-modal").click(function () {
    $.ajax({
        type: "GET",
        url: "password-modal.html",
        dataType : 'html',
        cache: false,
        success : function(data){
            $('body').append(data);
            $("#passwordModal").modal();

            $(".pass").keyup(function () {
                $(this).parent().next().find(".pass").focus();
            });
        }
    });
});