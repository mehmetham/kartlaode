$("#footer").load("footer.html");// Bu kod kaldırılacak.
$("#header").load("header.html"); // Bu kod kaldırılacak.


var isMobile = require('./lib/isMobile');
device = isMobile();
//footer ve header html olarak inlclude ettiğimiz için yazılım tarafında Settimeout function kaldırılacak.
setTimeout(function () {
    require('./lib/methods');
    require('./lib/header');
    require('./lib/validatorAndMask');


    var dropMenu = require('./lib/drop-menu');
    dpmenu = new dropMenu();

    var accordion = require('./lib/accordion');
    accordion = new accordion();

    $('.choose-payment-items .items  label').on('click', function () {
        $(".choose-payment-items .items").removeClass("active");
        $(this).closest(".items").addClass("active")
    });

    $('.radios-items-choose .items  label').on('click', function () {
        $(".radios-items-choose .items").removeClass("active");
        $(this).closest(".items").addClass("active")
    });

    if ($('.placeholder-animation').length > 0) {
        $('.placeholder-animation').placeholder({
            activeClass: 'text-input-placeholder_pos_top'
        });
    }


    if ($('#dtBasicExample').length > 0) {
        $('#dtBasicExample').DataTable({
            "searching": false,
            "info": false,
            "paging": false
        });
        $('.dataTables_length').addClass('bs-select');
    }

    $(".send-receipt-modal").click(function () {
        $.ajax({
            type: "GET",
            url: "send-receipt-modal.html",
            dataType: 'html',
            cache: false,
            success: function (data) {
                $('body').append(data);
                $("#receiptModal").modal();

                $('.placeholder-animation').placeholder({
                    activeClass: 'text-input-placeholder_pos_top'
                });
            }
        });
    });


    $(".passwordIcon").click(function () {
        var type = $(this).parents(".form-group").find("input").attr("type");

        if(type == "password"){
            $(this).parents(".form-group").find("input").attr("type","text");
            $(this).removeClass("font-icon-eye").addClass("font-icon-eye-clicked");
        }else{
            $(this).parents(".form-group").find("input").attr("type","password");
            $(this).removeClass("font-icon-eye-clicked").addClass("font-icon-eye");

        }
    });

    $(".promotion-modal").click(function () {
        $.ajax({
            type: "GET",
            url: "promotion-modal.html",
            dataType: 'html',
            cache: false,
            success: function (data) {
                $('body').append(data);
                $("#promotionModal").modal();

                $('.placeholder-animation').placeholder({
                    activeClass: 'text-input-placeholder_pos_top'
                });

                valid();
                mask();
            }
        });
    });


    if($('.datepicker').length > 0){
        var $datepicker = $('.datepicker');

        $datepicker.datepicker({
            'onSelect': function() {

                if ($('.placeholder-animation').length > 0) {
                    $('.placeholder-animation').placeholder({
                        activeClass: 'text-input-placeholder_pos_top'
                    });
                }
            }
        });
    }





}, 500);

valid = function () {


if ($("form").length > 0) {

    $('form').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            textbox: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            tc_no: {
                validators: {
                    stringLength: {
                        min:11,
                    },
                    integer: {
                        message: 'Please supply your first name'
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            promotion: {
                validators: {
                    stringLength: {
                        min:3,
                    },
                    integer: {
                        message: 'Please supply your first name'
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            cvv: {
                validators: {
                    stringLength: {
                        min:3,
                    },
                    integer: {
                        message: 'Please supply your first name'
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            vergi_no: {
                validators: {
                    stringLength: {
                        min:10,
                    },
                    integer: {
                        message: 'Please supply your first name'
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            iban: {
                validators: {
                    stringLength: {
                        min:32,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            price: {
                validators: {
                    stringLength: {
                        min:1,
                    },
                    integer: {
                        message: 'Please supply your first name'
                    }
                }
            },
            password: {
                validators: {
                    identical: {
                        field: 'confirmPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            last_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            phoneNumber: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
            address: {
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Please supply your street address'
                    }
                }
            },
            city: {
                validators: {
                    stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Please supply your city'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Please select your state'
                    }
                }
            },
            termsBox: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your city'
                    }
                }

            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your zip code'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Please supply a vaild zip code'
                    }
                }
            },
            comment: {
                validators: {
                    stringLength: {
                        min: 10,
                        max: 200,
                        message: 'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                }
            }
        }
    })
        .on('success.form.bv', function (e) {
            $('form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function (result) {
                console.log(result);
            }, 'json');
        });

}
}

valid();